# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer:
pkgname=py3-fonttools
_pkgname=fonttools
pkgver=4.37.4
pkgrel=0
pkgdesc="Converts OpenType and TrueType fonts to and from XML"
url="https://github.com/fonttools/fonttools"
arch="all"
license="MIT AND OFL-1.1"
depends="python3"
makedepends="
	cython
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-fs py3-pytest py3-pytest-xdist py3-brotli"
subpackages="$pkgname-doc"
source="$_pkgname-$pkgver.tar.gz::https://github.com/fonttools/fonttools/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-fonttools" # Backwards compatibility
provides="py-fonttools=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare
	# remove interpreter line
	sed -i '1d' Lib/fontTools/mtiLib/__init__.py
}

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	PATH="$PATH:$PWD" \
	PYTHONPATH=$(echo "$PWD"/build/lib*) \
	pytest -n4
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/fonttools-$pkgver-*.whl
}

doc() {
	replaces="py-$_pkgname-doc" # Backwards compatibility
	provides="py-$_pkgname-doc=$pkgver-r$pkgrel" # Backwards compatibility
	default_doc
}

sha512sums="
9e9c49732ca1e16a8d5b294423939297278d544456b37f5fd3b1f3eece8d9100834f8d7a8dee5908006a27419fbda06180ac588de5046a59fb243d728314939c  fonttools-4.37.4.tar.gz
"
